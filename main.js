


const lighthouse = require('lighthouse');
const chromeLauncher = require('chrome-launcher');

const fs = require('fs');

function launchChromeAndRunLighthouse(url, opts, config = null) {
  return chromeLauncher.launch({ chromeFlags: opts.chromeFlags }).then(chrome => {
    opts.port = chrome.port;
    return lighthouse(url, opts, config).then(results => {
      // use results.lhr for the JS-consumeable output
      // https://github.com/GoogleChrome/lighthouse/blob/master/types/lhr.d.ts
      // use results.report for the HTML/JSON/CSV output as a string
      // use results.artifacts for the trace/screenshots/other specific case you need (rarer)
      return chrome.kill().then(() => results.lhr)
    });
  });
}

const opts = {
  chromeFlags: ['--show-paint-rects']
};

// Usage:
// launchChromeAndRunLighthouse('https://www.bupa.com.au', opts).then(results => {
//   // Use results!
//   console.log(results);
//   fs.writeFileSync('auditData.json', JSON.stringify(results));
// });



let filter = ['audits'];

launchChromeAndRunLighthouse('https://www.bupa.com.au', opts) // run lighthouse
.then(results => filterData(results, filter))  // filtering for the audits object
.then(results => getKeys(results.audits)) // getting keys of audits object
  .then(filteredData => saveDataToFile('AuditObjectKeys.json',filteredData));


function getKeys(object) {
  return Object.keys(object);
}


function filterData(data, filters) {
  let returnData = {};
  filters.forEach(key => returnData[`${key}`] = data[key]);
  return returnData;
}

function saveDataToFile(fileName, data) {
    return fs.writeFileSync(`./savedData/${fileName}`, JSON.stringify(data));
    
}